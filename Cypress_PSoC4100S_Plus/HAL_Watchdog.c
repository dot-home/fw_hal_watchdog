//********************************************************************************
/*!
\author     Kraemer E.
\date       05.02.2019

\file       Watchdog.c
\brief      Watchdog handling

***********************************************************************************/
#include "TargetConfig.h"
#ifdef CYPRESS_PSOC4100S_PLUS


/********************************* includes **********************************/
#include "HAL_Watchdog.h"

/************************* local function prototypes *************************/
static u16 CalculateWatchdogTimeInterval(u16 uiTimerInterval);
static u16 CalculateSystemTimeInterval(u16 uiTimerInterval, u8 ucTimerIdx);

/************************* local data (const and var) ************************/

/************************ export data (const and var) ************************/
/* counting the amount for entering the Watchdog interrupt service routine (ISR) */
u8 ucWatchdogIsrCount = 0;

tsSystemTimers sSystemTimers[] =
{
    #define SYST(TimerName, TimerCnt, TimerMask, TimerInt, TimerReset, Callback) {TimerCnt, TimerMask, TimerInt, TimerReset, Callback},
        WDT_LIST
    #undef SYST
};

//********************************************************************************
/*!
\author  Kraemer E
\date    01.04.2021
\brief   Calculates the WDT interval from the given time(ms). When the time is 
         greater than the possible interval of the WDT, the interval is limited.
\param   ulTimerInterval - Time interval in milliseconds
\return  ulWDTInterval - The watchdog compare value
***********************************************************************************/
static u16 CalculateWatchdogTimeInterval(u16 uiTimerInterval)
{
    u32 ulWDTInterval = 0;
    
    /* Calculate WDT interval with following formula: Interval = (Lfclk * DesiredTimeMilliSecond)/ FactorForSecond */
    ulWDTInterval = (uiTimerInterval * LFCLK_ILO_CYCLES_PER_SECOND)/1000;
    
    /* Limit the interval value to the max WDT count */
    ulWDTInterval &= CY_SYS_WDT_MATCH_MASK;
    
    return (u16)ulWDTInterval;
}

//********************************************************************************
/*!
\author  Kraemer E
\date    01.04.2021
\brief   Calculates the system timer interval from the given time(ms). When the time is 
         greater than the possible interval of the system time, the interval is limited.
\param   ulTimerInterval - Time interval in milliseconds
\return  ulSysTimerInterval - The system time compare value
***********************************************************************************/
static u16 CalculateSystemTimeInterval(u16 uiTimerInterval, u8 ucTimerIdx)
{
    u32 ulSysTimerInterval = 0;
 
    if(ucTimerIdx < eSysTimer2)
    {
        /* Calculate system timer interval with following formula: Interval = (Lfclk * DesiredTimeMilliSecond)/ FactorForSecond */
        ulSysTimerInterval = (uiTimerInterval * LFCLK_WCO_CYCLES_PER_SECOND)/1000;
        
        /* Limit the interval value to the max WDT count (is equal to the system timer limits) */
        ulSysTimerInterval &= CY_SYS_WDT_MATCH_MASK;
    }
    else if(ucTimerIdx == eSysTimer2)
    {
        //TODO: Calculate value per bitshift. Timer 2 uses a 32bit-Toggle register where each bit
        //represents a time value. Formula: t = (2^n / WCO) where n is the shifted value   
    }
    return (u16)ulSysTimerInterval;
}


/*********************************  functions *********************************/
//********************************************************************************
/*!
\author  Kraemer E
\date    15.01.2019
\brief   interrupt service routine to handle watchdog interrupt, clear interrupt flag
         in normal working or store log data into flash when program is out of control
\param   none
\return  none
***********************************************************************************/
void WatchdogTimerInterruptRoutine(void)
{         
    /* Get the interrupt source address (SysTimer0, SysTimer1 or SysTimer2) */
    u32 ulSysTimerInterruptSrc = CySysTimerGetInterruptSource();        
            
    /* When the interrupt source is empty than the WDT has encountered */
    if(ulSysTimerInterruptSrc == 0)
    {
        if(ucWatchdogIsrCount == 0)
        {
            /* clear interrupt flag */
            CySysWdtClearInterrupt();                
        }
        else
        {        
            /*stop the ISR response for following interrupt */
            ISR_WDT_Stop();
            
            /* do not clear interrupt flag for Watchdog interrupt. The system reset
               is generated at the third interrrupt */
        }
        
        /* increase this counter to indentify if clearing watchdog works well */
        ucWatchdogIsrCount++;
    }
    
    /* Check all system timers for interrupt source */
    for(u8 ucSysTimerIdx = CY_SYS_NUM_OF_TIMERS; ucSysTimerIdx--;)
    {
        /* Check if system timer has set an interrupt */
        if(ulSysTimerInterruptSrc & sSystemTimers[ucSysTimerIdx].ulTimerInt)
        {       
            //Clear the system timer flag to enable the next interrupt
            CySysTimerClearInterrupt(sSystemTimers[ucSysTimerIdx].ulTimerInt);
            
            //Check if a callback function was set for this timer
            if(sSystemTimers[ucSysTimerIdx].pCallbackFct)
            {
                //Call callback function
                sSystemTimers[ucSysTimerIdx].pCallbackFct();
            }
        }
    }
    
    ISR_WDT_ClearPending();
    ISR_WDT_WCO_ClearPending();
}


//********************************************************************************
/*!
\author  Kraemer E
\date    15.01.2019
\brief   Initialize watchdog counter 0 with specific reset interval
\param   uiResetInterval - reset interval, in ms unit
\return  none
***********************************************************************************/   
teSysTimerReturn HAL_WDT_InitWatchdog(u16 uiResetInterval)
{    
    teSysTimerReturn eReturn = eSysTimer_Success;
    
    /* Before a change has any effect the WDT has to be disabled */
    if(CySysWdtGetEnabledStatus())
    {
        CySysWdtDisable();
    }     

    /* Calculate the match-compare value for the WDT */
    uiResetInterval = CalculateWatchdogTimeInterval(uiResetInterval);
        
    if(uiResetInterval >= CY_SYS_WDT_MATCH_MASK)
    {
        eReturn = eSysTimer_LimitReached;
    }
    
    /* Set desired interval value */
	CySysWdtSetMatch(uiResetInterval);
    
    /* Clear the watchdog interrupt */
    CySysWdtClearInterrupt();

    /* Enable WDT interrupt */
    CySysWdtUnmaskInterrupt();

    //If necessary... use Global signal reference instead
    //CySysWdtSetInterruptCallback(WatchdogTimerInterruptRoutine);
    
    //Let the WDT some time to settle down 
    CyDelayUs(200);
                
    /* Enable the WDT */           
    CySysWdtEnable();     
    
    /* Set watchdog counter ISR to custom interrupt service */
    ISR_WDT_StartEx(WatchdogTimerInterruptRoutine);
    ISR_WDT_SetPriority(0); 
    
    return eReturn;
}

//********************************************************************************
/*!
\author  Kraemer E
\date    10.10.2020
\brief   Enables or disables the system timer
\param   eSystemTimer - System timer which shall be enabled/disabled
\param   bEnableDisable - True to enable system timer and false to disable timer
\return  none
***********************************************************************************/
teSysTimerReturn HAL_WDT_SystemTimerDisableEnable(teSystemTimers eSystemTimer, bool bEnableDisable)
{   
    teSysTimerReturn eReturn = eSysTimer_Invalid;
    
    if(eSystemTimer < eSysTimerLastEntry)
    {
        /* Enable global signal ref ISR for this timer */
        ISR_WDT_WCO_Enable();
        
        if(bEnableDisable == ON)
        {
            /* Enable system timer only when its not running */
            if(CySysTimerGetEnabledStatus(sSystemTimers[eSystemTimer].ulTimerCnt) == false)
            {
                CySysTimerEnable(sSystemTimers[eSystemTimer].ulTimerMask);
                eReturn = eSysTimer_Success;
            }    
            else
            {
                eReturn = eSysTimer_TimerAlreadyEnabled;
            }
        }
        else
        {
            /* Disable system timer only when its running */
            if(CySysTimerGetEnabledStatus(sSystemTimers[eSystemTimer].ulTimerCnt) == true)
            {
                CySysTimerDisable(sSystemTimers[eSystemTimer].ulTimerMask);
                eReturn = eSysTimer_Success;
            }
            else
            {
                eReturn = eSysTimer_TimerAlreadyDisabled;
            }
        }
    }
    
    return eReturn;
}


//********************************************************************************
/*!
\author  Kraemer E
\date    10.10.2020
\brief   Resets the system timer value
\param   eSystemTimer - The system timer which shall be reseted
\return  none
***********************************************************************************/
teSysTimerReturn HAL_WDT_SystemTimerResetCounter(teSystemTimers eSystemTimer)
{    
    teSysTimerReturn eReturn = eSysTimer_Invalid;
    
    if(eSystemTimer < eSysTimerLastEntry)
    {
        /* Clear system timer only when its running */
        if(CySysTimerGetEnabledStatus(sSystemTimers[eSystemTimer].ulTimerCnt) == true)
        {
            CySysTimerResetCounters(sSystemTimers[eSystemTimer].ulTimerReset);
            eReturn = eSysTimer_Success;
        }
        else
        {
            eReturn = eSysTimer_TimerIsDisabled;
        }
    }
    
    return eReturn;    
}


//********************************************************************************
/*!
\author  Kraemer E
\date    28.02.2019
\brief   Clears counter of the watchdog timer
\param   none
\return  none
***********************************************************************************/
void HAL_WDT_ClearWatchdogCounter(void)
{
    /* Clear Watchdog CNT*/
    CySysWdtClearInterrupt();
}


//********************************************************************************
/*!
\author  Kraemer E
\date    10.10.2020
\brief   Initializes the system timer with the given capture compare value.
         Timers are set in interrupt mode with clear on match!
\param   eSystemTimer - The desired system timer
\param   uiTimerValue - The timer value for the timeout in milliseconds.
\return  bInitDone - Returns true when system timer could be initialized
***********************************************************************************/
teSysTimerReturn HAL_WDT_SystemTimerInit(teSystemTimers eSystemTimer, u16 uiTimerValue)
{   
    teSysTimerReturn eReturn = eSysTimer_Invalid;
    
    /* Check if requested timer is WDT0 or WDT1. WDT2 needs another init routine */
    if(eSystemTimer < eSysTimer2)
    {
        /* Use pointer arritmetic for easier handling */
        const tsSystemTimers* psSysTimer = &sSystemTimers[eSystemTimer];
        
        /* Check first if timer is running */
        if(CySysTimerGetEnabledStatus(psSysTimer->ulTimerCnt) == true)
        {
            /* When running disable timer */
            CySysTimerDisable(psSysTimer->ulTimerMask);
        }
        
        //Disable servicing interrupts from system timer
        CySysTimerDisableIsr(psSysTimer->ulTimerCnt);
        
        /* Configure system timer to run in interrupt mode and enable clear on match. */
        CySysTimerSetMode(psSysTimer->ulTimerCnt, CY_SYS_TIMER_MODE_INT);
        
        /* Calculate capture value from the given time value */
        uiTimerValue = CalculateSystemTimeInterval(uiTimerValue, eSystemTimer);
        
        if(uiTimerValue >= CY_SYS_WDT_MATCH_MASK)
        {
            eReturn = eSysTimer_LimitReached;
        }
        
        CySysTimerSetMatch(psSysTimer->ulTimerCnt, uiTimerValue);
        CySysTimerSetClearOnMatch(psSysTimer->ulTimerCnt, true);   
        
        CyDelayUs(200);
        
        /* Enable timer again */
        CySysTimerEnable(psSysTimer->ulTimerMask);
        
        /* Enable system timer ISR */
        CySysTimerEnableIsr(psSysTimer->ulTimerCnt);    
        
        /* WHen the return value is still invalid set it to success */
        if(eReturn == eSysTimer_Invalid)
        {
            eReturn = eSysTimer_Success;
        }
    }
    
    return eReturn;
}


//********************************************************************************
/*!
\author  Kraemer E
\date    10.10.2020
\brief   Initializes the system timer as a free running timer. In this mode no 
         capture compare value is requiered.
\param   eSystemTimer - The desired system timer
\return  bInitDone - Returns true when system timer could be initialized
***********************************************************************************/
teSysTimerReturn HAL_WDT_SystemTimerInitFreeRunning(teSystemTimers eSystemTimer)
{
    teSysTimerReturn eReturn = eSysTimer_Invalid;
    
    /* Check if requested timer is WDT0 or WDT1. WDT2 needs another init routine */
    if(eSystemTimer < eSysTimer2)
    {
        /* Use pointer arritmetic for easier handling */
        const tsSystemTimers* psSysTimer = &sSystemTimers[eSystemTimer];
        
        /* Check first if timer is running */
        if(CySysTimerGetEnabledStatus(psSysTimer->ulTimerCnt) == true)
        {
            /* When running disable timer */
            CySysTimerDisable(psSysTimer->ulTimerMask);
        }
        
        /* Check if WCO is already running */
        if(CySysClkWcoEnabled() == false)
        {
            /* Start WCO */
            CySysClkWcoStart();
        }
        
        //Disable servicing interrupts from system timer
        CySysTimerDisableIsr(psSysTimer->ulTimerCnt);
    
        /* Configure system timer to run in free-running mode */
        CySysTimerSetMode(psSysTimer->ulTimerCnt, CY_SYS_TIMER_MODE_NONE);
        
        CyDelayUs(200);
    
        /* Enable timer again */
        CySysTimerEnable(psSysTimer->ulTimerMask);
        
        /* Enable system timer ISR */
        //CySysTimerEnableIsr(psSysTimer->ulTimerCnt);    
        
        /* WHen the return value is still invalid set it to success */
        if(eReturn == eSysTimer_Invalid)
        {
            eReturn = eSysTimer_Success;
        }
    }
    
    return eReturn;
}

//********************************************************************************
/*!
\author  Kraemer E
\date    10.10.2020
\brief   Set a new callback function for the system timer
\param   eSystemTimer - The system timer which shall get a callback function
\param   pCallbackFunction - The callback function which shall be used
\return  none
***********************************************************************************/
teSysTimerReturn HAL_WDT_SystemTimerSetCallbackFunction(teSystemTimers eSystemTimer, void* pCallbackFunction)
{
    teSysTimerReturn eReturn = eSysTimer_InvalidTimer;
    
    if(pCallbackFunction && eSystemTimer < eSysTimerLastEntry)
    {
        sSystemTimers[eSystemTimer].pCallbackFct = pCallbackFunction;
        eReturn = eSysTimer_Success;
    }
    
    return eReturn;
}

//********************************************************************************
/*!
\author  Kraemer E
\date    10.10.2020
\brief   Initializes the WCO interrupt for the system timer
\param   none
\return  none
***********************************************************************************/
void HAL_WDT_SystemTimerInterruptInit(void)
{
    /* Set watchdog-timer counter ISR to custom interrupt service */
    ISR_WDT_WCO_Disable();
    ISR_WDT_WCO_StartEx(WatchdogTimerInterruptRoutine);
    ISR_WDT_WCO_SetPriority(0);
    ISR_WDT_WCO_Enable(); 
}


//********************************************************************************
/*!
\author  Kraemer E
\date    10.10.2020
\brief   Enables or disables the WCO interrupt
\param   bEnableDisable - true when WCO interrupt shall be enabled. Otherwise false
\return  none
***********************************************************************************/
void HAL_WDT_SystemTimerInterruptEnableDisable(bool bEnableDisable)
{
    if(bEnableDisable == ON)
    {
        ISR_WDT_WCO_Enable(); 
    }
    else
    {
        ISR_WDT_WCO_Disable(); 
    }
}

//********************************************************************************
/*!
\author  Kraemer E
\date    03.05.2021
\brief   Reads the current timer tick count
\param   eSystemTimer - The timer name which shall be checked
\return  ulTick - Current tick count of the desired system timer
***********************************************************************************/
u32 HAL_WDT_SystemTimerGetTick(teSystemTimers eSystemTimer)
{
    u32 ulTick = ULONG_MAX;
    
    if(eSystemTimer < eSysTimerLastEntry)
    {
        ulTick = CySysTimerGetCount(sSystemTimers[eSystemTimer].ulTimerCnt);
    }
    
    return ulTick;
}

//********************************************************************************
/*!
\author  Kraemer E
\date    03.05.2021
\brief   Gets the running status of the timer.
\param   eSystemTimer - The timer name which shall be checked
\return  eReturn - Returns the status of the timer.
***********************************************************************************/
teSysTimerReturn HAL_WDT_SystemTimerGetRunningStatus(teSystemTimers eSystemTimer)
{
    teSysTimerReturn eReturn = eSysTimer_Invalid;
    
    /* Check if requested timer is WDT0 or WDT1. WDT2 needs another init routine */
    if(eSystemTimer < eSysTimer2)
    {        
        /* Check first if timer is running */
        if(CySysTimerGetEnabledStatus(sSystemTimers[eSystemTimer].ulTimerCnt) == true)
        {
            eReturn = eSysTimer_TimerRunning;
        }
        else
        {
            eReturn = eSysTimer_TimerSuspended;
        }        
    }
    
    return eReturn;
}

#endif //PSOC_4100S_PLUS