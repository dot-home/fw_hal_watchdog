//********************************************************************************
/*!
\author     Kraemer E.
\date       05.02.2019

\file       Watchdog.h
\brief      Watchdog header file

***********************************************************************************/
#ifndef _HAL_WATCHDOG_H_
#define _HAL_WATCHDOG_H_

#ifdef __cplusplus
extern "C"
{
#endif

/********************************* includes **********************************/
#include "BaseTypes.h"
#include "HAL_Config.h"

/***************************** defines / macros ******************************/

// Generate an enum list for the system timer list
typedef enum
{
    #ifdef WDT_LIST
        #define SYST(TimerName, TimerCnt, TimerMask, TimerInt, TimerReset, Callback) TimerName,
            WDT_LIST
        #undef SYST
    #endif
    eSysTimerLastEntry
}teSystemTimers;

typedef enum
{
    eSysTimer_Success,              /* When requested operation was successful */
    eSysTimer_LimitReached,         /* When the used timeout is greater than the limit */
    eSysTimer_InvalidTimer,         /* When the used timer index is invalid */
    eSysTimer_TimerAlreadyEnabled,  /* When a timer should be enabled which is already enabled */
    eSysTimer_TimerAlreadyDisabled, /* When a timer should be disabled altough it's already disabled */
    eSysTimer_TimerIsDisabled,      /* When a timer shall be used which is disabled */
    eSysTimer_TimerRunning,         /* Status when the timer is running */
    eSysTimer_TimerSuspended,       /* Status when the timer is supended */
    eSysTimer_Invalid               /* Fault couldn't be derived */
}teSysTimerReturn;

typedef struct
{
    u32 ulTimerCnt;             /* The timer number */
    u32 ulTimerMask;            /* System timer mask */
    u32 ulTimerInt;             /* System timer interrupt mask */
    u32 ulTimerReset;           /* System timer reset mask */
    pFunction pCallbackFct;     /* Callback function which shall be called when timeout occurs for this timer */
}tsSystemTimers;

extern tsSystemTimers sSystemTimers[];

/************************ externally visible functions ***********************/
teSysTimerReturn HAL_WDT_InitWatchdog(u16 uiResetInterval);
void HAL_WDT_ClearWatchdogCounter(void);

teSysTimerReturn HAL_WDT_SystemTimerInit(teSystemTimers eSystemTimer, u16 uiTimerValue);
teSysTimerReturn HAL_WDT_SystemTimerInitFreeRunning(teSystemTimers eSystemTimer);
teSysTimerReturn HAL_WDT_SystemTimerDisableEnable(teSystemTimers eSystemTimer, bool bEnableDisable);
teSysTimerReturn HAL_WDT_SystemTimerResetCounter(teSystemTimers eSystemTimer);
teSysTimerReturn HAL_WDT_SystemTimerSetCallbackFunction(teSystemTimers eSystemTimer, void* pCallbackFunction);
teSysTimerReturn HAL_WDT_SystemTimerGetRunningStatus(teSystemTimers eSystemTimer);
u32 HAL_WDT_SystemTimerGetTick(teSystemTimers eSystemTimer);


void HAL_WDT_SystemTimerInterruptInit(void);
void HAL_WDT_SystemTimerInterruptEnableDisable(bool bEnableDisable);

#ifdef __cplusplus
}
#endif

#endif // _HAL_WATCHDOG_H_

/* [] END OF FILE */
