//********************************************************************************
/*!
\author     Kraemer E.
\date       05.02.2019

\file       Watchdog.c
\brief      Watchdog handling

***********************************************************************************/
#include "TargetConfig.h"
#ifdef ESPRESSIF_ESP8266


/********************************* includes **********************************/
#include "HAL_Watchdog.h"

/************************* local function prototypes *************************/

/************************* local data (const and var) ************************/

/************************ export data (const and var) ************************/


/*********************************  functions *********************************/



//********************************************************************************
/*!
\author  Kraemer E
\date    15.01.2019
\brief   Initialize watchdog counter 0 with specific reset interval
\param   uiResetInterval - reset interval, in ms unit
\return  none
***********************************************************************************/   
teSysTimerReturn HAL_WDT_InitWatchdog(u16 uiResetInterval)
{    
    teSysTimerReturn eReturn = eSysTimer_Success;
    return eReturn;
}

//********************************************************************************
/*!
\author  Kraemer E
\date    10.10.2020
\brief   Enables or disables the system timer
\param   eSystemTimer - System timer which shall be enabled/disabled
\param   bEnableDisable - True to enable system timer and false to disable timer
\return  none
***********************************************************************************/
teSysTimerReturn HAL_WDT_SystemTimerDisableEnable(teSystemTimers eSystemTimer, bool bEnableDisable)
{   
    teSysTimerReturn eReturn = eSysTimer_Success;
       
    return eReturn;
}


//********************************************************************************
/*!
\author  Kraemer E
\date    10.10.2020
\brief   Resets the system timer value
\param   eSystemTimer - The system timer which shall be reseted
\return  none
***********************************************************************************/
teSysTimerReturn HAL_WDT_SystemTimerResetCounter(teSystemTimers eSystemTimer)
{    
    teSysTimerReturn eReturn = eSysTimer_Success;
   
    return eReturn;    
}


//********************************************************************************
/*!
\author  Kraemer E
\date    28.02.2019
\brief   Clears counter of the watchdog timer
\param   none
\return  none
***********************************************************************************/
void HAL_WDT_ClearWatchdogCounter(void)
{

}


//********************************************************************************
/*!
\author  Kraemer E
\date    10.10.2020
\brief   Initializes the system timer with the given capture compare value.
         Timers are set in interrupt mode with clear on match!
\param   eSystemTimer - The desired system timer
\param   uiTimerValue - The timer value for the timeout in milliseconds.
\return  bInitDone - Returns true when system timer could be initialized
***********************************************************************************/
teSysTimerReturn HAL_WDT_SystemTimerInit(teSystemTimers eSystemTimer, u16 uiTimerValue)
{   
    teSysTimerReturn eReturn = eSysTimer_Success;
    
    return eReturn;
}


//********************************************************************************
/*!
\author  Kraemer E
\date    10.10.2020
\brief   Initializes the system timer as a free running timer. In this mode no 
         capture compare value is requiered.
\param   eSystemTimer - The desired system timer
\return  bInitDone - Returns true when system timer could be initialized
***********************************************************************************/
teSysTimerReturn HAL_WDT_SystemTimerInitFreeRunning(teSystemTimers eSystemTimer)
{
    teSysTimerReturn eReturn = eSysTimer_Success;
    
    return eReturn;
}

//********************************************************************************
/*!
\author  Kraemer E
\date    10.10.2020
\brief   Set a new callback function for the system timer
\param   eSystemTimer - The system timer which shall get a callback function
\param   pCallbackFunction - The callback function which shall be used
\return  none
***********************************************************************************/
teSysTimerReturn HAL_WDT_SystemTimerSetCallbackFunction(teSystemTimers eSystemTimer, void* pCallbackFunction)
{
    teSysTimerReturn eReturn = eSysTimer_Success;

    return eReturn;
}

//********************************************************************************
/*!
\author  Kraemer E
\date    10.10.2020
\brief   Initializes the WCO interrupt for the system timer
\param   none
\return  none
***********************************************************************************/
void HAL_WDT_SystemTimerInterruptInit(void)
{
//No WDT used
}


//********************************************************************************
/*!
\author  Kraemer E
\date    10.10.2020
\brief   Enables or disables the WCO interrupt
\param   bEnableDisable - true when WCO interrupt shall be enabled. Otherwise false
\return  none
***********************************************************************************/
void HAL_WDT_SystemTimerInterruptEnableDisable(bool bEnableDisable)
{
//No WDT used
}

//********************************************************************************
/*!
\author  Kraemer E
\date    03.05.2021
\brief   Reads the current timer tick count
\param   eSystemTimer - The timer name which shall be checked
\return  ulTick - Current tick count of the desired system timer
***********************************************************************************/
u32 HAL_WDT_SystemTimerGetTick(teSystemTimers eSystemTimer)
{
    //No WDT used
    return 0;
}

//********************************************************************************
/*!
\author  Kraemer E
\date    03.05.2021
\brief   Gets the running status of the timer.
\param   eSystemTimer - The timer name which shall be checked
\return  eReturn - Returns the status of the timer.
***********************************************************************************/
teSysTimerReturn HAL_WDT_SystemTimerGetRunningStatus(teSystemTimers eSystemTimer)
{
    teSysTimerReturn eReturn = eSysTimer_Success;
    
    //No WDT used
    return eReturn;
}

#endif //PSOC_4100S_PLUS